#include <iostream>
#include <string>
using namespace std;

int main() {
  
  string phoneNumber;

  cout << "Enter 11 digit phone nubmer: ";
  cin >> phoneNumber;
  
  if (phoneNumber.length() < 11) {
    cout << "Number too short.";
  } else if (phoneNumber.length() > 11) {
    cout << "Number too long.";
    }

  cout << "Your number is: (" << phoneNumber.substr(0, 3) << ") "
       << phoneNumber.substr(4, 4) << " " << phoneNumber.substr(7, 4) << endl;

  return 0;
}
  
