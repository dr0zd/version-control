#include <iostream>
using namespace std;

int main() {

  int digit1;
  int digit2;
  int digit3;

  cout << "Enter 3 integers and program will put them in descending order." << endl;
  cout << "Enter first integer: ";
  cin >> digit1;
  cout << "Enter second integer: ";
  cin >> digit2;
  cout << "Enter third integer: ";
  cin >> digit3;

  if (digit1 > digit2 && digit1 > digit3 && digit2 > digit3) {
    cout << "The order is: " << digit1 << " " << digit2 << " "  << digit3 << endl;
  } else if (digit1 > digit2 && digit1 > digit3 && digit2 < digit3) {
    cout << "The order is: " << digit1 << " " << digit3 << " " << digit2 << endl;  
  } else if (digit1 < digit2 && digit1 > digit3 && digit2 > digit3) {
    cout << "The order is: " << digit2 << " " << digit1 << " " << digit3 << endl;
  } else if (digit1 < digit2 && digit1 < digit3 && digit2 > digit3) {
    cout << "The order is: " << digit2 << " " << digit3 << " " << digit1 << endl; 
  } else if (digit1 > digit2 && digit1 < digit3 && digit2 < digit3) {
    cout << "The order is: " << digit3 << " " << digit1 << " " << digit2 << endl;
  } else if (digit1 < digit2 && digit1 < digit3 && digit2 < digit3) {
    cout << "The order is: " << digit3 << " " << digit2 << " " << digit1 << endl;
  } else {
    cout << "Some form of error... or two..." << endl;
  }
  return 0;
}
  
