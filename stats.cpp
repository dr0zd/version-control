#include <iostream>
using namespace std;

int main() {
  int x;
  int y;
  cout << "Enter 2 numbers: ";
  cin >> x >> y;
  cout << x << " + " << y << " = " << x + y << endl;
  cout << x << " - " << y << " = " << x - y << endl;
  cout << x << " * " << y << " = " << x * y << endl;
  
  return 0;
}
